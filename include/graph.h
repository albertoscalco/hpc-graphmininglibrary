# pragma once

#include <vector>

namespace gml
{
    typedef size_t node_id_t;

/// Graph
///
/// Abstract graph.
///
/// NOTE: you can subclass this if you want to
///       implement optimized data structures for
///       indexing the graph and to support optimized
///       mining algorithms.
    class Graph
    {
    public:
        /// Adds a list of neighbours to the current graph.
        ///
        /// \param t_from Source node id.
        /// \param t_neighbours List of outgoing edges.
        virtual void addNeighbours(const node_id_t t_from, const std::vector<node_id_t> &t_neighbours) = 0;

        /// Adds an edge to the current graph.
        ///
        /// \param t_from Source node id.
        /// \param t_to Destination node id.
        virtual void addEdge(const node_id_t t_from, const node_id_t t_to) = 0;

        /// Is the graph directed or not.
        bool isDirected() const { return m_is_directed; }

        /// Get the number of nodes in the graph.
        virtual size_t nodes() const = 0;

        /// Get the number of edges in the graph.
        virtual size_t edges() const = 0;

        /// Get the number of out-going links.
        ///
        /// \param node node id.
        /// \return number of out-going links.
        virtual size_t outDegree(node_id_t node) const = 0;

        /// Get the number of in-coming links.
        ///
        /// \param node node id.
        /// \return number of in-coming links.
        virtual size_t inDegree(node_id_t node) const = 0;

        /// Get neighbours of a given node.
        ///
        /// \param t_from Source node id.
        /// \return a const vector of neighbours ids.
        ///
        /// NOTE: methods similar to this one, or other methods
        ///       may be added at will if required to implement some
        ///       optimized mining algorithm.
        virtual const node_id_t *neighbours(node_id_t t_from) const = 0;

    private:

        bool m_is_directed;  /// True if graph is directed
    };


} // namespace gml
