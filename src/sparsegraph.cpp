#include <string>

#include "sparsegraph.h"

namespace gml
{

    SparseGraph::SparseGraph(const std::string &t_filename)
    {

    }

    void SparseGraph::addNeighbours(const node_id_t t_from, const std::vector<node_id_t> &t_neighbours)
    {

    }

    void SparseGraph::addEdge(const node_id_t t_from, const node_id_t t_to)
    {

    }

    size_t SparseGraph::inDegree(node_id_t node) const
    {
        return 0;
    }


} // namespace gml
