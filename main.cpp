#include <iostream>


#include "sparsegraph.h"

int main()
{
    gml::SparseGraph s_graph("test");
    s_graph.addEdge(1, 2);
    s_graph.addNeighbours(1, {1, 2, 3});
    std::cout << s_graph.nodes();

    return 0;
}