# pragma once

#include <vector>

#include "graph.h"

namespace gml
{


    typedef size_t node_id_t;

/// SparseGraph
///
/// Efficient storage/indexing of a sparse graph.
    class SparseGraph : public Graph
    {
    public:
        /// Constructor
        /// Loads a sparse graph from the given filename.
        ///
        /// \param t_filename name of the file storing the graph.
        ///                 Snap Dataset format is used.
        SparseGraph(const std::string &t_filename);

        /// Adds a list of neighbours to the current graph.
        ///
        /// \param t_from Source node id.
        /// \param t_neighbours List of outgoing edges.
        void addNeighbours(const node_id_t t_from, const std::vector<node_id_t> &t_neighbours);

        /// Adds an edge to the current graph.
        ///
        /// \param t_from Source node id.
        /// \param t_to Destination node id.
        void addEdge(const node_id_t t_from, const node_id_t t_to);

        /// Get the number of nodes in the graph.
        virtual size_t nodes() const { return m_adj.size(); }

        /// Get the number of edges in the graph.
        virtual size_t edges() const { return m_edges; }

        /// Get the number of out-going links.
        ///
        /// \param node node id.
        /// \return number of out-going links.
        virtual size_t outDegree(node_id_t node) const { return m_adj[node].size(); };

        /// Get the number of in-coming links.
        ///
        /// \param node node id.
        /// \return number of in-coming links.
        virtual size_t inDegree(node_id_t node) const;

        /// Get neighbours of a given node.
        ///
        /// \param t_from Source node id.
        /// \return a const vector of neighbours ids.
        virtual const node_id_t *neighbours(node_id_t t_from) const { return &(m_adj[t_from][0]); }

    private:

        std::vector<std::vector<node_id_t>> m_adj; /// Adjacency lists

        size_t m_edges;  /// number of edges

    };


} // namespace gml
